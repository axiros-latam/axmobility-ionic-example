package io.ionic.starter;

import com.axiros.axmobility.AxEvents;
import com.axiros.axmobility.android.AxMobility;
import com.axiros.axmobility.android.AxSettings;
import com.axiros.axmobility.android.settings.CPEID;
import com.axiros.axmobility.android.settings.Notification;
import com.axiros.axmobility.android.taskmanager.TaskType;
import com.axiros.axmobility.events.CoreEvents;
import com.axiros.axmobility.events.CycleEvents;
import com.axiros.axmobility.events.TR143DetailEvents;
import com.axiros.axmobility.events.TR143Events;
import com.axiros.axmobility.tr143.Details;
import com.axiros.axmobility.tr143.InProgress;
import com.axiros.axmobility.tr143.TR143Diagnostic;
import com.axiros.axmobility.type.LogLevel;
import com.getcapacitor.JSObject;
import com.getcapacitor.PermissionState;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.CapacitorPlugin;
import com.getcapacitor.annotation.Permission;
import com.getcapacitor.annotation.PermissionCallback;

import android.Manifest;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;


@CapacitorPlugin(
  name = "AxMobilityPlugin",
  permissions = {@Permission(strings = {
    Manifest.permission.ACCESS_COARSE_LOCATION,
    Manifest.permission.ACCESS_FINE_LOCATION,
    Manifest.permission.ACCESS_BACKGROUND_LOCATION,
    Manifest.permission.INTERNET,
    Manifest.permission.ACCESS_NETWORK_STATE,
    Manifest.permission.READ_PHONE_STATE,
    Manifest.permission.ACCESS_WIFI_STATE,
    Manifest.permission.CHANGE_WIFI_STATE,
  }, alias = "all-permissions")}
)
public class AxMobilityPlugin extends Plugin {
  public static final String AXM_KEY = "PUT_YOUR_KEY_HERE";

  @PluginMethod()
  public void startAxMobility(final PluginCall call) {
    if (getPermissionState("all-permissions") != PermissionState.GRANTED) {
      requestAllPermissions(call, "start");
    } else {
      start(call);
    }
  }

  @PermissionCallback
  private void start(PluginCall call) {
    final AxEvents events = new AxEvents.Builder()
      .withTR143Events(new TR143Events() {
        @Override
        public void onBegin(TR143Diagnostic tr143Diagnostic) {
        }

        @Override
        public void onCompleted(TR143Diagnostic tr143Diagnostic) {
        }
      })
      .withTR143DetailEvents(new TR143DetailEvents() {
        @Override
        public void inProgress(TR143Diagnostic tr143Diagnostic, InProgress inProgress) {
        }

        @Override
        public void details(TR143Diagnostic tr143Diagnostic, Details details) {
        }
      })
      .build();

    try {
      String clientVersion = "1.0.0";
      TaskType taskType = TaskType.DEFERRABLE;
      AxSettings settings = AxSettings.Builder()
        .withKey(AXM_KEY)
        .withClientVersion(clientVersion)
        .withClientName("IonicAxMobility")
        .withLogTag("com.axiros.ionic")
        .withTaskType(taskType)
        .withEvents(events)
        .build();

      /* Starts the library */
      AxMobility.start(getContext(), settings);
      JSObject ret = new JSObject();
      ret.put("cpeid", AxMobility.getCpeID());
      call.resolve(ret);
    } catch (Exception e) {
      Log.e("com.axiros.ionic", "Client MainActivity axmobilityStart()", e);
      call.reject("Error starting library");
    }
  }
}



