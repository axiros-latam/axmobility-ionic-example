import { WebPlugin } from '@capacitor/core';

import type { AxMobilityPlugin } from './definitions';

export class AxMobilityWeb extends WebPlugin implements AxMobilityPlugin {
  startAxMobility(): Promise<any> {
    return Promise.resolve(undefined);
  }

}
