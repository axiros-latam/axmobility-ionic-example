import { Component } from '@angular/core';
import { registerPlugin } from '@capacitor/core';
import {AxMobilityPlugin} from '../definitions';

const axMobilityPlugin = registerPlugin<AxMobilityPlugin>('AxMobilityPlugin');
export default axMobilityPlugin;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

export class AppComponent {
  constructor() {}
}
