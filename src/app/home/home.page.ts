import {Component} from '@angular/core';
import {Capacitor} from '@capacitor/core';
import axMobilityPlugin from '../app.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  cpeid = 'Not Available';
  constructor() {}

  // eslint-disable-next-line @angular-eslint/use-lifecycle-interface
  ngOnInit(): void {
    console.log('ngOnInit');
    if (Capacitor.isPluginAvailable('AxMobilityPlugin')) {
      console.log('plugin is available');
      this.startAxMobility().then(value => {
        this.cpeid = value.cpeid;
      }).catch(reason => {
        console.log(reason);
      });
    } else {
      console.log('plugin is NOT available');
    }
  }
  private async startAxMobility(): Promise<any> {
    return await axMobilityPlugin.startAxMobility();
  }
}
