# AxMobility Ionic Example

An AxMobility (minimal) example using Ionic framework.

## Pre-requisites

* npm

## Build process

To build the example, first execute the following command at the repository root path:

* ionic capacitor sync android

Then, open the android folder into Android Studio to build the APK.

